from src.business_components.resources_plan import *
from unittest import TestCase
from src.create_data import *

resource_plan = ResourcesPlan(data_worker)


class TestRescourcePlan(TestCase):
    def test_component(self):
        project_id = pr5_id
        resources_info = resource_plan.get_resources_info(project_id)
        print('')
        print('Initial resources info')
        print(str(resources_info))
        print('')

        works_ids = resource_plan.data_worker.get_works_in_project(project_id)

        self.assertEqual(len(resources_info), 2)
        self.assertEqual(resources_info[works_ids[0]][0][1], 10)

        resource_plan.change_resource(project_id, 1, works_ids[0], 0, 20, 0)
        print('After changing resource 0')
        resources_info = resource_plan.get_resources_info(project_id)
        print(str(resources_info))
        print('')

        self.assertEqual(len(resources_info[works_ids[1]]), 1)
        self.assertEqual(resources_info[works_ids[0]][0][1], 20)

        resource_plan.change_resource(project_id, 1, works_ids[1], 1, 5, 0)
        print('After changing resource 1')
        resources_info = resource_plan.get_resources_info(project_id)
        print(str(resources_info))
        print('')

        self.assertEqual(resources_info[works_ids[1]][0][1], 5)
