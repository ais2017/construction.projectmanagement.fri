from src.classes.system import *
from src.classes.project import *
from src.classes.worker import *
from src.classes.work import *
from src.classes.resource import *
from unittest import TestCase

from datetime import date, datetime
from src.create_data import *


class TestProject(TestCase):
    def test_class_methods(self):
        pr1 = Project()
        rights = SystemRights()
        rights.add_rights_for_projects(pr1, 'description', Access.ALLOWED)
        self.assertEqual(pr1.get_works_plan(), [])
        pr1.set_customer('Serious Customer', rights)
        self.assertEqual(pr1.get_customer(), 'Serious Customer')

        manager = SystemUser('Main Manager', 'MM', 'howyoudoin')

        pr2 = Project(manager, 'Serious Customer', '10.11.2017', '10.11.2018')
        self.assertEqual(pr2.get_start_of_construction(), datetime.strptime('10.11.2017', '%d.%m.%Y').date())
        self.assertEqual(pr2.get_deadline_of_construction(), datetime.strptime('10.11.2018', '%d.%m.%Y').date())

        rights.add_rights_for_projects(pr2, 'description', Access.ALLOWED)
        pr2.set_description('Very important project', rights)
        self.assertEqual(pr2.get_description(), 'Very important project')

        pr1.set_start_of_construction('01.10.2017', rights)
        self.assertEqual(pr1.get_start_of_construction(), datetime.strptime('01.10.2017', '%d.%m.%Y').date())

        w1 = Work('Work 1', '10.11.2017, 09:00', '10.11.2017, 10:00')
        rights.add_rights_for_projects(pr2, 'work', Access.ALLOWED)
        pr2.add_work(w1, rights)
        self.assertEqual(len(pr2.get_works_plan()), 1)

        pr2.remove_work(w1, rights)
        self.assertEqual(len(pr2.get_works_plan()), 0)

        pr2.add_work(w1, rights)

        w2 = Work('Work 2', '10.11.2017, 10:00', '10.11.2017, 12:00')
        rights.add_rights_for_works(w1, 'works', Access.ALLOWED)
        w1.add_work_after(w2, rights)

        pr2.set_work_plan([w1, w2], rights)
        self.assertEqual(len(pr2.get_works_plan()), 2)

        wr1 = Worker('Hand Worker', 100)
        rights.add_rights_for_projects(pr2, 'work', Access.ALLOWED)
        rights.add_rights_for_works(w1, 'assignments', Access.ALLOWED)
        pr2.add_worker(wr1, w1, 0.85, rights)
        assignments2 = pr2.get_assignments_info(rights)
        wr1_found = False
        self.assertNotEqual(assignments2, None)
        self.assertNotEqual(len(assignments2), 0)
        for w in assignments2:
            for a in assignments2[w]:
                if a.get_worker() == wr1:
                    wr1_found = True
        self.assertEqual(wr1_found, True)

        pr2.remove_worker(wr1, w1, rights)
        self.assertEqual(len(pr2.get_assignments_info(rights)[w1]), 0)

        r1 = Resource(200)
        rights.add_rights_for_works(w1, 'resources', Access.ALLOWED)
        pr2.add_resource(r1, 10, w1, rights)
        self.assertEqual(len(pr2.get_resources_info()), 2)
        self.assertEqual(len(pr2.get_resources_info()[w1]), 1)

        pr2.remove_resource(r1, w1, rights)
        self.assertEqual(len(pr2.get_resources_info()), 2)

        rights.add_rights_for_works(w1, 'description', Access.ALLOWED)
        rights.add_rights_for_works(w2, 'description', Access.ALLOWED)
        w1.set_start_of_realization(datetime(2017, 10, 1), rights)
        w1.set_deadline(datetime(2017, 11, 1), rights)
        w2.set_start_of_realization(datetime(2017, 11, 1), rights)
        w2.set_deadline(datetime(2017, 11, 12), rights)
        self.assertEqual(pr2.calculate_delay().days, (datetime.now() - w2.get_deadline()).days)

        rights.add_rights_for_projects(pr2, 'done_work', Access.ALLOWED)
        pr2.add_done_work([w2], rights)
        self.assertEqual(len(pr2.get_done_works()), 1)


