from src.business_components.creation_gantt_diagram import *
from unittest import TestCase
from src.create_data import *

cr_gantt = CreateGanttDiagram(data_worker)


class TestGantt(TestCase):
    def test_creation(self):
        work0 = ('Work0', '10.10.2017, 09:00', '10.11.2017, 19:00')
        work1 = ('Work1', '10.11.2017, 19:00', '20.11.2017, 19:00')
        work2 = ('Work2', '10.11.2017, 19:00', '11.11.2017, 14:00')
        work3 = ('Work3', '11.11.2017, 11:00', '11.11.2017, 14:00')
        work4 = ('Work4', '11.11.2017, 14:00', '11.11.2017, 15:00')

        works_descriptions = [work0, work1, work2, work3, work4]
        works_connections = [{'fs': [1]},
                            {'ss': [2]},
                            {'ff': [3]},
                            {},
                            {'sf': [3]}]

        cr_gantt.create_gantt_diagram(1, 1, works_descriptions, works_connections)

        self.assertEqual(len(cr_gantt.data_worker.projects[1].get_works_plan()), 5)
        self.assertEqual(cr_gantt.data_worker.projects[1].work_plan[2].name, 'Work2')
        self.assertEqual(len(cr_gantt.data_worker.projects[1].work_plan[2].works_finish_to_finish), 1)


