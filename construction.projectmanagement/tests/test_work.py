import sys
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
cur_path = dir_path.split('/')[-1]
dir_path = dir_path.split('/' + cur_path)[0]
sys.path.append(dir_path)

from src.classes.resource import *
from src.classes.work import *
from src.classes.worker import *
from src.classes.system import *
from unittest import TestCase

from datetime import date, datetime, time


class TestWork(TestCase):

    def test_work_methods(self):
        w1 = Work('Work 1', '10.10.2017, 09:00', '10.11.2017, 09:00')

        rq1 = ResourceQuantity(100)
        rights = SystemRights()
        rights.add_rights_for_works(w1, 'resources', Access.ALLOWED)
        w1.add_resource(rq1, rights)
        self.assertEqual(len(w1.get_resources()), 1)

        rq2 = ResourceQuantity(100)
        w1.add_resource(rq2, rights)
        self.assertEqual(w1.calculate_cost(), 2)

    def test_worker_methods(self):
        wr1 = Worker('Hard Worker', 100)
        self.assertEqual(wr1.get_fullname(), 'Hard Worker')
        wr1.set_max_effort_units_per_day(1.0)
        self.assertEqual(wr1.get_max_effort_units_per_day(), 1.0)
        cur_year = date.today().year
        cur_month = date.today().month
        wr1.add_time_of_work(date(cur_year, cur_month, 1), [[time(9, 0), time(13, 0)], [time(14, 0), time(18, 0)]])
        self.assertEqual(len(wr1.get_dates_of_works()), 1)
        self.assertEqual(len(wr1.get_time_of_work(date(cur_year, cur_month, 1))), 2)
