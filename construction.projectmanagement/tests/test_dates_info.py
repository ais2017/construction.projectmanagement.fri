from src.business_components.dates_info import *
from unittest import TestCase
from src.create_data import *

dates_info = DatesInfo(data_worker)


class TestRescourcePlan(TestCase):
    def test_component(self):
        project_id = pr3_id
        dates = dates_info.get_dates_info(project_id)
        self.assertEqual(dates[0].strftime('%d.%m.%Y'), '10.10.2017')
        self.assertEqual(dates[1].strftime('%d.%m.%Y'), '10.10.2040')

        dates_info.set_deadline_of_construction(1, 1, '10.10.2050')
        dates = dates_info.get_dates_info(1)
        self.assertEqual(dates[1].strftime('%d.%m.%Y'), '10.10.2050')
