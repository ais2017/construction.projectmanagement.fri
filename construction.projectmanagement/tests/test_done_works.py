from src.business_components.done_works import *
from unittest import TestCase
from src.create_data import *

done_works = DoneWorks(data_worker)


class TestDoneWorks(TestCase):
    def test_component(self):
        br_id = 2
        project_id = pr4_id

        project_info = done_works.get_project_info(project_id)
        self.assertEqual(project_info[0], 'Trump')

        works, works_connections = done_works.get_real_work_plan(project_id)

        self.assertEqual(len(works), 2)
        self.assertEqual(len(works_connections), 2)
        self.assertEqual(len(works_connections[list(works.keys())[0]]['fs']), 1)
        self.assertEqual(works[2][1], '10.10.2017, 09:00')
        self.assertEqual(works[2][3], None)

        start = datetime.now().strftime('%d.%m.%Y, %H:%M')
        done_works.set_real_start(br_id, 2, start)
        works, _ = done_works.get_real_work_plan(project_id)
        work = done_works.data_worker.get_work(2)
        self.assertEqual(work.get_real_start(), datetime.strptime(start, '%d.%m.%Y, %H:%M'))

        done_works.add_done_works(br_id, project_id, [2])
        project = done_works.data_worker.get_project(project_id)
        self.assertEqual(len(project.get_done_works()), 1)

        end = datetime.now().strftime('%d.%m.%Y, %H:%M')
        done_works.set_real_end(br_id, 2, end)
        works, _ = done_works.get_real_work_plan(project_id)
        work = done_works.data_worker.get_work(2)
        self.assertEqual(work.get_real_end(), datetime.strptime(end, '%d.%m.%Y, %H:%M'))




