from src.business_components.general_info import *
from unittest import TestCase
from src.create_data import *

gen_info = GeneralInfo(data_worker)


class TestGeneralInfo(TestCase):
    def test_component(self):
        boss_id = 3

        accessible_projects = gen_info.get_all_accessible_projects(2)
        self.assertEqual(len(accessible_projects), 1)

        project_id = 1
        project_info = gen_info.get_project_info(project_id)
        # print(str(project_info))
        self.assertEqual(project_info[0], 'Hollywood')
        self.assertEqual(project_info[2], '10.10.2050')

        new_project_info = (project_info[0], project_info[1], '10.10.2019', project_info[-1])

        gen_info.change_project_info(boss_id, project_id, new_project_info)
        project_info = gen_info.get_project_info(project_id)
        self.assertEqual(project_info[2], '10.10.2019')
