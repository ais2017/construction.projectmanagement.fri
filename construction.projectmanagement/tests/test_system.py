import sys
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
cur_path = dir_path.split('/')[-1]
dir_path = dir_path.split('/' + cur_path)[0]
sys.path.append(dir_path)

from src.classes.project import *
from unittest import TestCase


class TestSystem(TestCase):

    def test_system_rights(self):
        manager = SystemUser()
        self.assertEqual(manager.fullname, '')

        manager = SystemUser('Ivanov I. I.', 'i123', '123')
        pr1 = Project()
        manager.rights.add_rights_for_projects(pr1, 'description', Access.ALLOWED)
        manager.rights.add_rights_for_projects(pr1, 'work', Access.ALLOWED)
        manager.rights.add_rights_for_projects(pr1, 'done_work', Access.FORBIDDEN)

        self.assertEqual(manager.rights.get_access_to_project_action(pr1, 'description'), Access.ALLOWED)
        self.assertEqual(manager.rights.get_access_to_project_action(pr1, 'done_work'), Access.FORBIDDEN)
        self.assertEqual(manager.rights.get_access_to_project_action(pr1, 'brigadiers'), Access.UNKNOWN)

        pr2 = Project()
        self.assertEqual(manager.rights.get_access_to_project_action(pr2, 'work'), Access.UNKNOWN)

        manager.rights.add_rights_for_projects(pr2, 'work', Access.ALLOWED)
        # print('manager.rights.rights_for_projects = ' + str(manager.rights.rights_for_projects))
        self.assertEqual(len(manager.rights.rights_for_projects), 2)

        with self.assertRaises(AccessError) as context:
            pr2.set_customer('Big Customer', manager.rights)

        manager.rights.add_rights_for_projects(pr2, 'description', Access.ALLOWED)
        pr2.set_customer('Big Customer', manager.rights)
        self.assertEqual(pr2.customer, 'Big Customer')
