from src.business_components.project_delays import *
from unittest import TestCase
from src.create_data import *

project_delays = ProjectDelays(data_worker)


class TestProjectDelays(TestCase):
    def test_component(self):
        project_id = pr3_id
        project = project_delays.data_worker.get_project(project_id)
        user_rights = project_delays.data_worker.get_user_rights(1)

        w1 = Work('Dreaming about summer', '01.12.2017, 09:00', '08.12.2017, 09:00')
        project.add_work(w1, user_rights)
        new_key1 = max(list(data_worker.works.keys())) + 1
        data_worker.works[new_key1] = w1
        w2 = Work('Dreaming about holidays', '04.12.2017, 09:00', '09.12.2017, 09:00')
        project.add_work(w2, user_rights)
        new_key2 = max(list(data_worker.works.keys())) + 1
        data_worker.works[new_key2] = w2

        self.assertEqual(project_delays.get_project_deadline(project_id).strftime('%d.%m.%Y'), '10.10.2019')
        self.assertEqual(project_delays.get_project_delay(project_id).days, (datetime.now() - data_worker.works[new_key2].deadline).days)
        self.assertEqual(project_delays.get_project_delay(project_id).seconds / 60,
                         (datetime.now() - data_worker.works[new_key2].deadline).seconds / 60)

        work_delays = project_delays.get_works_delays(3)
        self.assertEqual(len(work_delays), 2)

        project.work_plan.remove(w1)
        project.work_plan.remove(w2)
        data_worker.works.pop(new_key1)
        data_worker.works.pop(new_key2)

