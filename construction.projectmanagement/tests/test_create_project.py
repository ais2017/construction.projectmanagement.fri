from src.business_components.creating_project import *
from unittest import TestCase
from src.create_data import *

from src.business_components.done_works import *

cr_project = CreateProject(data_worker)
dw = DoneWorks(data_worker)


class TestProjectCreation(TestCase):
    def test_component(self):
        manager_id = 1

        project_id = cr_project.fill_project_fields(manager_id, 'Google', '01.09.2017', '01.10.2019')

        work0 = ('Work0', '10.10.2017, 09:00', '10.11.2017, 19:00')
        work1 = ('Work1', '10.11.2017, 19:00', '20.11.2017, 19:00')
        work2 = ('Work2', '10.11.2017, 19:00', '11.11.2017, 14:00')
        work3 = ('Work3', '11.11.2017, 11:00', '11.11.2017, 14:00')
        work4 = ('Work4', '11.11.2017, 14:00', '11.11.2017, 15:00')

        works_descriptions = [work0, work1, work2, work3, work4]
        works_connections = [{'fs': [1]},
                            {'ss': [2]},
                            {'ff': [3]},
                            {},
                            {'sf': [3]}]

        cr_project.create_work_plan(manager_id, project_id, works_descriptions, works_connections)

        not_here_work = -1
        here_works = []
        for w_id in data_worker.works:
            if data_worker.works[w_id] not in data_worker.projects[project_id].work_plan:
                not_here_work = w_id
            else:
                here_works.append(w_id)

        with self.assertRaises(LogicError) as context:
            cr_project.create_assignment(manager_id, project_id, 1, not_here_work, 1.00)

        self.assertTrue('This work is not in the plan for the project!' in str(context.exception))

        cr_project.create_assignment(manager_id, project_id, 1, here_works[0], 1.00)
        cr_project.create_assignment(manager_id, project_id, 2, here_works[1], 0.75)

        cr_project.add_resource(manager_id, project_id, 1, 10, here_works[0])

        project = cr_project.data_worker.get_project(project_id)
        self.assertEqual(len(project.get_works_plan()), 5)
        self.assertEqual(len(project.get_assignments_info(cr_project.data_worker.get_user_rights(manager_id))), 5)

        a_info = project.get_assignments_info(cr_project.data_worker.get_user_rights(manager_id))
        for w_name in a_info:
            if w_name == cr_project.data_worker.get_work(3).name:
                self.assertEqual(len(a_info[w_name]), 1)

        r_info = project.get_resources_info()
        self.assertEqual(len(r_info), 5)
        for w_name in r_info:
            if w_name == cr_project.data_worker.get_work(3).name:
                self.assertEqual(len(r_info[w_name]), 1)
                self.assertEqual(r_info[w_name][0].quantity, 10)
