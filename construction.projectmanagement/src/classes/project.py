from datetime import date, timedelta

from src.classes.resource import Resource, ResourceQuantity
from src.classes.system import Access, SystemRights
from src.classes.system import SystemUser, ObjectWithRights
from src.classes.work import Work
from src.classes.worker import Worker, Assignment

from datetime import date, datetime

from src.utils.errors import *


class Project(ObjectWithRights):
    def __init__(self, manager: SystemUser = None, customer: str = '',
                 start_of_construction: str = '01.01.1971', deadline_of_construction: str = '01.01.1971'):
        self.manager = manager

        # description access
        self.customer = customer
        self.start_of_construction = datetime.strptime(start_of_construction, '%d.%m.%Y').date()
        self.deadline_of_construction = datetime.strptime(deadline_of_construction, '%d.%m.%Y').date()
        self.description = ''

        # brigadiers access
        self.brigadiers = []

        # work access
        self.work_plan = []

        # done_work access
        self.done_work = []

        self.resources = []

    def set_customer(self, customer: str, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'description' in accesses and accesses['description'] == Access.ALLOWED:
            self.customer = customer
        else:
            raise AccessError()

    def get_customer(self):
        return self.customer

    def set_description(self, description: str, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'description' in accesses and accesses['description'] == Access.ALLOWED:
            self.description = description
        else:
            raise AccessError()

    def get_description(self):
        return self.description

    def set_start_of_construction(self, start_of_construction: date, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'description' in accesses and accesses['description'] == Access.ALLOWED:
            self.start_of_construction = datetime.strptime(start_of_construction, '%d.%m.%Y').date()
        else:
            raise AccessError()

    def get_start_of_construction(self):
        return self.start_of_construction

    def set_deadline_of_construction(self, deadline_of_construction: date, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'description' in accesses and accesses['description'] == Access.ALLOWED:
            self.deadline_of_construction = datetime.strptime(deadline_of_construction, '%d.%m.%Y').date()
        else:
            raise AccessError()

    def get_deadline_of_construction(self):
        return self.deadline_of_construction

    def add_work(self, work: Work, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'work' in accesses and accesses['work'] == Access.ALLOWED:
            self.work_plan.append(work)
        else:
            raise AccessError()

    def remove_work(self, work: Work, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'work' in accesses and accesses['work'] == Access.ALLOWED:
            self.work_plan.remove(work)
        else:
            raise AccessError()

    def add_brigadier(self, worker: Worker, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'brigadiers' in accesses and accesses['brigadiers'] == Access.ALLOWED:
            self.brigadiers.append(worker)
        else:
            raise AccessError()

    def remove_brigadier(self, worker: Worker, accesses: SystemRights):
        if 'brigadiers' in accesses and accesses['brigadiers'] == Access.ALLOWED:
            self.brigadiers.remove(worker)
        else:
            raise AccessError()

    def add_worker(self, worker: Worker, work: Work, effort_units: int, accesses: SystemRights):
        accesses_ = accesses.get_project_accesses(self)
        if 'work' in accesses_ and accesses_['work'] == Access.ALLOWED:
            if work not in self.work_plan:
                raise LogicError('This work is not in the plan for the project!')
            else:
                assignment = Assignment(worker, effort_units)
                work.add_assignment(assignment, accesses)
                return assignment
        else:
            raise AccessError()

    def remove_worker(self, worker, work, accesses: SystemRights):
        accesses_ = accesses.get_project_accesses(self)
        if 'work' in accesses_ and accesses_['work'] == Access.ALLOWED:
            if work not in self.work_plan:
                raise LogicError('This work is not in the plan for the project!')
            else:
                work.remove_worker(worker, accesses)
        else:
            raise AccessError()

    def add_resource(self, resource: Resource, quantity: int, work: Work, accesses: SystemRights):
        accesses_ = accesses.get_project_accesses(self)
        if 'work' in accesses_ and accesses_['work'] == Access.ALLOWED:
            if work not in self.work_plan:
                raise LogicError('This work is not in the plan for the project!')
            else:
                work.add_resource(ResourceQuantity(resource, quantity), accesses)
        else:
            raise AccessError()

    def set_resource(self, resource: Resource, quantity: int, spent: int, work: Work, accesses: SystemRights):
        accesses_ = accesses.get_project_accesses(self)
        if 'work' in accesses_ and accesses_['work'] == Access.ALLOWED:
            if work not in self.work_plan:
                raise LogicError('This work is not in the plan for the project!')
            else:
                rq = ResourceQuantity(resource, quantity)
                rq.spent_quantity = spent
                work.set_resource(rq, accesses)
        else:
            raise AccessError()

    def remove_resource(self, resource: Resource, work: Work, accesses: SystemRights):
        accesses_ = accesses.get_project_accesses(self)
        if 'work' in accesses_ and accesses_['work'] == Access.ALLOWED:
            if work not in self.work_plan:
                raise LogicError('This work is not in the plan for the project!')
            else:
                work.remove_resource(resource, accesses)
        else:
            raise AccessError()

    def calculate_delay(self) -> timedelta:
        if len(self.work_plan) == 0:
            return timedelta(0)
        else:
            delay = timedelta(0)
            for w in self.work_plan:
                if w not in self.done_work:
                    cur_delay = w.get_delay()
                    if delay == timedelta(0) or cur_delay < delay:
                        delay = cur_delay
            return delay

    def set_work_plan(self, work_plan: list, accesses: SystemRights):
        if len(self.work_plan) != 0:
            self.work_plan.clear()
        for w in work_plan:
            if w.start_of_realization.date() < self.start_of_construction or \
                            w.deadline.date() > self.deadline_of_construction:
                raise LogicError('Work ' + w.get_name() + ' dates are not in the period of projects dates')
        self.work_plan = work_plan

    def add_done_work(self, done_works: list, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'done_work' in accesses and accesses['done_work'] == Access.ALLOWED:
            for dw in done_works:
                if dw not in self.work_plan:
                    raise LogicError('This work is not in the plan for the project!')
                if dw not in self.done_work:
                    self.done_work += done_works
        else:
            raise AccessError()

    def get_done_works(self) -> list:
        return self.done_work

    def get_works_plan(self) -> list:
        return self.work_plan

    def get_resources_info(self):  # returns dict of {work_name: list of resource_to_quantity}
        all_rq = {}

        for w in self.work_plan:
            all_rq[w] = []
            for rq in w.get_resources():
                all_rq[w].append(rq)

        return all_rq

    def get_assignments_info(self, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'work' in accesses and accesses['work'] == Access.ALLOWED:
            all_assignments = {}

            for w in self.work_plan:
                all_assignments[w] = []
                for a in w.get_assignments():
                    all_assignments[w].append(a)
            return all_assignments
        else:
            raise AccessError()

    def get_view_info(self):
        return self.customer, self.start_of_construction.strftime('%d.%m.%Y'), \
               self.deadline_of_construction.strftime('%d.%m.%Y'), self.description

    # info is in the order self.customer, self.start_of_construction, self.deadline_of_construction, self.description
    def set_info(self, info: tuple, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'description' in accesses and accesses['description'] == Access.ALLOWED:
            self.customer = info[0]
            self.start_of_construction = datetime.strptime(info[1], '%d.%m.%Y').date()
            self.deadline_of_construction = datetime.strptime(info[2], '%d.%m.%Y').date()
            self.description = info[3]
        else:
            raise AccessError()
