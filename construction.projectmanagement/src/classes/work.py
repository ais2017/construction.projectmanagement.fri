from datetime import datetime, timedelta

from src.classes.resource import ResourceQuantity, Resource
from src.classes.worker import Assignment, Worker
from src.classes.system import ObjectWithRights, Access, SystemRights

from src.utils.errors import *


class Work(ObjectWithRights):
    def __init__(self, name: str = '', start_of_realization: str = '01.01.1971, 00:00',
                 deadline: str = '01.01.1971, 00:01', resource_to_quantity: list = None,
                 assignments: list = None):
        super(Work, self).__init__()

        # description access
        self.name = name
        self.start_of_realization = datetime.strptime(start_of_realization, '%d.%m.%Y, %H:%M')
        self.deadline = datetime.strptime(deadline, '%d.%m.%Y, %H:%M')

        if self.start_of_realization > self.deadline:
            raise LogicError('Start of realization date cannot be later than deadline')

        # resources
        if resource_to_quantity is None:
            self.resource_to_quantity = []
        else:
            self.resource_to_quantity = resource_to_quantity

        # assignments access
        if assignments is None:
            self.assignments = []
        else:
            self.assignments = assignments

        # times access
        self.real_start_of_realization = None
        self.real_end_of_realization = None

        # works access
        self.works_start_to_start = []  # parallel start works
        self.works_finish_to_start = []  # works after this
        self.works_finish_to_finish = []  # parallel end works
        self.works_start_to_finish = []  # works before this

    def set_name(self, name: str, accesses: SystemRights):
        accesses = accesses.get_project_accesses(self)
        if 'description' in accesses and accesses['description'] == Access.ALLOWED:
            self.name = name
        else:
            raise AccessError()

    def get_name(self):
        return self.name

    def get_deadline(self):
        return self.deadline

    def calculate_cost(self) -> int:
        cost = 0
        for rq in self.resource_to_quantity:
            cost += rq.calculate_cost()
        for assignment in self.assignments:
            cost += assignment.calculate_cost()
        return cost

    def add_work_parallel_start(self, work, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'works' in accesses and accesses['works'] == Access.ALLOWED:
            if self.start_of_realization != work.start_of_realization:
                raise LogicError('Start to start works must start at the same time')
            self.works_start_to_start.append(work)
        else:
            raise AccessError()

    def remove_work_parallel_start(self, work, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'works' in accesses and accesses['works'] == Access.ALLOWED:
            self.works_start_to_start.remove(work)

    def add_work_parallel_end(self, work, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'works' in accesses and accesses['works'] == Access.ALLOWED:
            if self.deadline != work.deadline:
                raise LogicError('Finish to finish works must end at the same time')
            self.works_finish_to_finish.append(work)
        else:
            raise AccessError()

    def remove_work_parallel_end(self, work, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'works' in accesses and accesses['works'] == Access.ALLOWED:
            self.works_finish_to_finish.remove(work)
        else:
            raise AccessError()

    def add_work_after(self, work, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'works' in accesses and accesses['works'] == Access.ALLOWED:
            if self.deadline != work.start_of_realization:
                raise LogicError('Finish to start works end and start must be equal')
            self.works_finish_to_start.append(work)
        else:
            raise AccessError

    def remove_work_after(self, work, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'works' in accesses and accesses['works'] == Access.ALLOWED:
            self.works_finish_to_start.remove(work)
        else:
            raise AccessError()

    def add_work_before(self, work, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'works' in accesses and accesses['works'] == Access.ALLOWED:
            if self.start_of_realization != work.deadline:
                raise LogicError('Start to finish works start and end must be equal')
            self.works_start_to_finish.append(work)
        else:
            raise AccessError()

    def remove_work_before(self, work, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'works' in accesses and accesses['works'] == Access.ALLOWED:
            self.works_start_to_finish.remove(work)
        else:
            raise AccessError()

    def add_resource(self, rq: ResourceQuantity, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'resources' in accesses and accesses['resources'] == Access.ALLOWED:
            self.resource_to_quantity.append(rq)
        else:
            raise AccessError()

    def set_resource(self, rq: ResourceQuantity, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'resources' in accesses and accesses['resources'] == Access.ALLOWED:
            for rq_ in self.resource_to_quantity:
                if rq_ == rq:
                    rq_.quantity = rq.quantity
                    return
            self.resource_to_quantity.append(rq)
        else:
            raise AccessError()

    def remove_resource(self, resource: Resource, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'resources' in accesses and accesses['resources'] == Access.ALLOWED:
            for rq in self.resource_to_quantity:
                if rq.resource == resource:
                    self.resource_to_quantity.remove(rq)
                    break
        else:
            raise AccessError()

    def get_resources(self) -> list:
        return self.resource_to_quantity

    def add_assignment(self, assignment: Assignment, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'assignments' in accesses and accesses['assignments'] == Access.ALLOWED:
            self.assignments.append(assignment)
        else:
            raise AccessError()

    def remove_worker(self, worker: Worker, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'assignments' in accesses and accesses['assignments'] == Access.ALLOWED:
            for a in self.assignments:
                if a.worker == worker:
                    self.assignments.remove(a)
                    break
        else:
            raise AccessError()

    def get_assignments(self) -> list:
        return self.assignments

    def set_start_of_realization(self, date: datetime, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'description' in accesses and accesses['description'] == Access.ALLOWED:
            self.start_of_realization = date
        else:
            raise AccessError()

    def get_start_of_realization(self):
        return self.start_of_realization

    def set_deadline(self, deadline: datetime, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'description' in accesses and accesses['description'] == Access.ALLOWED:
            self.deadline = deadline
        else:
            raise AccessError()

    def set_real_start(self, date: datetime, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'times' in accesses and accesses['times'] == Access.ALLOWED:
            self.real_start_of_realization = date
        else:
            raise AccessError()

    def get_real_start(self):
        return self.real_start_of_realization

    def get_real_end(self):
        return self.real_end_of_realization

    def set_real_end(self, date: datetime, accesses: SystemRights):
        accesses = accesses.get_work_accesses(self)
        if 'times' in accesses and accesses['times'] == Access.ALLOWED:
            self.real_end_of_realization = date
        else:
            raise AccessError()

    def get_delay(self):
        if self.real_end_of_realization is None:
            return datetime.now() - self.deadline
        else:
            return timedelta(0)
