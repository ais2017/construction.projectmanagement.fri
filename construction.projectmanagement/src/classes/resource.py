class Resource:
    def __init__(self, quantity_in_storage):
        self.quantity_in_storage = quantity_in_storage


class ResourceQuantity:
    def __init__(self, resource: Resource, quantity: int = 0):
        self.resource = resource
        self.quantity = quantity
        self.spent_quantity = 0

    def calculate_cost(self):
        return 1

    def add(self, quantity: int):
        self.quantity += quantity
