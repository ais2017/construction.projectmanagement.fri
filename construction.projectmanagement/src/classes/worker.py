from datetime import date

from src.classes.system import ObjectWithRights, Access


class Worker:
    def __init__(self, fullname: str = '', salary: int = 0):
        self.fullname = fullname
        self.salary = salary  # per day if effort units = 1.0
        self.times_of_works = {}  # date in current month -> times of work: list. Each time of work is list of
        # 2 elements of type datetime.time where first one is the start of time period, second is the end.
        self.max_effort_units_per_day = 0.0  # percentage of work the worker can do per day (max is 1.0)

    def get_max_effort_units_per_day(self) -> float:
        return self.max_effort_units_per_day

    # def get_effort_units_per_day(self):

    def get_dates_of_works(self) -> list:
        return list(self.times_of_works.keys())

    def get_time_of_work(self, day: date) -> list:
        return self.times_of_works[day]

    def get_salary(self) -> int:
        return self.salary

    def set_salary(self, salary: int):
        self.salary = salary

    def get_fullname(self):
        return self.fullname

    def set_fullname(self, fullname):
        self.fullname = fullname

    def add_time_of_work(self, date_of_work: date, times_of_works: list):
        self.times_of_works[date_of_work] = times_of_works

    def set_max_effort_units_per_day(self, max_effort_units_per_day: float):
        self.max_effort_units_per_day = max_effort_units_per_day


class Assignment(ObjectWithRights):
    def __init__(self, worker: Worker = None, effort_units: float = 0.0):
        # general access
        self.worker = worker
        self.effort_units = effort_units

    def get_effort_units(self) -> float:
        return self.effort_units

    def set_worker(self, worker: Worker, accesses: dict):
        if 'general' in accesses and accesses['general'] == Access.ALLOWED:
            self.worker = worker

    def get_worker(self):
        return self.worker

    def set_effort_units(self, effort_units: float, accesses: dict):
        if 'general' in accesses and accesses['general'] == Access.ALLOWED:
            self.effort_units = effort_units

    def calculate_cost(self) -> float:
        return self.worker.get_salary() * self.effort_units
