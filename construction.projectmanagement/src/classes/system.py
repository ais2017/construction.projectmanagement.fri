from enum import Enum

from src.utils.errors import *


class ObjectWithRights:
    pass


class Access(Enum):
    ALLOWED = 0
    FORBIDDEN = 1
    UNKNOWN = 2


class SystemRightsForObject:
    def __init__(self, object_with_rights: ObjectWithRights, actions_to_accesses: dict):
        if len(actions_to_accesses) < 1:
            raise LogicError('Accesses to actions list cannot be empty')
        for access in actions_to_accesses.values():
            if type(access) is not Access:
                raise AttributeError()

        self.object = object_with_rights
        self.actions_to_accesses = actions_to_accesses

    def add_access_to_action(self, action_id, access: Access):
        self.actions_to_accesses[action_id] = access

    def get_access_to_action(self, action_id):
        if action_id in self.actions_to_accesses:
            return self.actions_to_accesses[action_id]
        return Access.UNKNOWN


class SystemRights:
    def __init__(self, rights_for_projects: list = None, rights_for_works: list = None, rights_for_resources: list = None,
                 rights_for_assignments: list = None):
        if rights_for_projects is None:
            self.rights_for_projects = []  # each of fields is list of SystemRightsForObjects
        else:
            self.rights_for_projects = rights_for_projects
        if rights_for_works is None:
            self.rights_for_works = []
        else:
            self.rights_for_works = rights_for_works
        if rights_for_resources is None:
            self.rights_for_resources = []
        else:
            self.rights_for_resources = rights_for_resources
        if rights_for_assignments is None:
            self.rights_for_assignments = []
        else:
            self.rights_for_assignments = rights_for_assignments

    def add_rights(self, rights_list: list, object: ObjectWithRights, action_id, access: Access):
        for sr in rights_list:
            if sr.object == object:
                sr.add_access_to_action(action_id, access)
                return
        rights_list.append(SystemRightsForObject(object, {action_id: access}))

    def get_access(self, rights_list: list, object: ObjectWithRights, action_id):
        for sr in rights_list:
            if sr.object == object:
                return sr.get_access_to_action(action_id)
        return Access.UNKNOWN

    def add_rights_for_projects(self, project: ObjectWithRights, action_id, access: Access):
        self.add_rights(self.rights_for_projects, project, action_id, access)

    def get_access_to_project_action(self, project: ObjectWithRights, action_id):
        return self.get_access(self.rights_for_projects, project, action_id)

    def add_rights_for_works(self, work: ObjectWithRights, action_id, access: Access):
        self.add_rights(self.rights_for_works, work, action_id, access)

    def get_access_to_work_action(self, work: ObjectWithRights, action_id):
        return self.get_access(self.rights_for_works, work, action_id)

    def add_rights_for_resources(self, resource: ObjectWithRights, action_id, access: Access):
        self.add_rights(self.rights_for_resources, resource, action_id, access)

    def get_access_to_resource_action(self, resource: ObjectWithRights, action_id):
        return self.get_access(self.rights_for_resources, resource, action_id)

    def add_rights_for_assignments(self, assignment: ObjectWithRights, action_id, access: Access):
        self.add_rights(self.rights_for_assignments, assignment, action_id, access)

    def get_access_to_assignment_action(self, assignment: ObjectWithRights, action_id):
        return self.get_access(self.rights_for_assignments, assignment, action_id)

    def get_project_accesses(self, project):
        for sr in self.rights_for_projects:
            if sr.object == project:
                return sr.actions_to_accesses.copy()
        return {}

    def get_work_accesses(self, work):
        for sr in self.rights_for_works:
            if sr.object == work:
                return sr.actions_to_accesses.copy()
        return {}

    def get_resource_accesses(self, resource):
        for sr in self.rights_for_resources:
            if sr.object == resource:
                return sr.actions_to_accesses.copy()
        return {}

    def get_assignment_accesses(self, assignment):
        for sr in self.rights_for_resources:
            if sr.object == assignment:
                return sr.actions_to_accesses.copy()
        return {}


class SystemUser(ObjectWithRights):
    def __init__(self, fullname: str = '', login: str = '', password: str = '', rights: SystemRights = None):
        super(SystemUser, self).__init__()
        
        # general access
        self.fullname = fullname
        self.login = login
        self.password = password
        if rights is None:
            self.rights = SystemRights()
        else:
            self.rights = rights

    def set_fullname(self, fullname: str, accesses: dict):
        if 'general' in accesses and accesses['general'] == Access.ALLOWED:
            self.fullname = fullname
        else:
            raise AccessError()

    def set_login(self, login: str, accesses: dict):
        if 'general' in accesses and accesses['general'] == Access.ALLOWED:
            self.login = login
        else:
            raise AccessError()

    def set_password(self, password: str, accesses: dict):
        if 'general' in accesses and accesses['general'] == Access.ALLOWED:
            self.password = password
        else:
            raise AccessError()

    def set_rights(self, rights: SystemRights, accesses: dict):
        if 'general' in accesses and accesses['general'] == Access.ALLOWED:
            self.password = rights

    def get_rights(self):
        return self.rights
