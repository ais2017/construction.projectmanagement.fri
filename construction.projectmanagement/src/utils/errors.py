class LogicError(Exception):
    def __init__(self, message):
        self.message = message


class AccessError(Exception):
    def __init__(self):
        self.message = 'Access is not allowed'
