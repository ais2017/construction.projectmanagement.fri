from src.classes.system import *
from src.classes.project import *
from copy import copy

import ast
import postgresql

# this is the interface, which must be implemented to get access to real database


class DataWorker:
    def get_user_rights(self, user_id: int):
        pass

    def get_user(self, user_id: int):
        pass

    def add_user_rights_for_project(self, user_id: int, object_id: int, rights: dict):
        pass

    def add_user_rights_for_work(self, user_id: int, object_id: int, rights: dict):
        pass

    def set_work_plan(self, user_id: int, project_id: int, work_plan: list, rights: SystemRights):
        pass

    def get_worker(self, worker_id: int):
        pass

    def add_project(self, project: Project):
        pass

    def get_work(self, work_id: int):
        pass

    def get_project(self, project_id: int):
        pass

    def get_resource_on_project_id(self, project_id: int, work_id: int, rs: Resource):
        pass

    def get_work_id(self, work: Work, project_id):
        pass

    def get_resource_on_base(self, res_id: int):
        pass

    def set_resource_on_project(self, project_id: int, work_id: int, resource_id: int, quantity: int, spent: int,
                                rights: SystemRights):
        pass

    # This method should return information about works in project (their ids), connections between them,
    # plan dates of realization and real dates of realization
    def get_real_work_plan(self, project_id: int):
        pass

    def add_assignment(self, project_id: int, worker_id: int, work_id: int, effort_units: float, rights: SystemRights):
        pass

    def add_resource_to_project(self, project_id: int, work_id: int, resource_id: int, quantity: int, rights: SystemRights):
        pass

    def get_resource_on_project(self, project_id: int, work_int: int, resource_id: int):
        pass

    def set_work_real_start(self, work_id: int, date_time: datetime):
        pass

    def set_work_real_end(self, work_id: int, date_time: datetime):
        pass

    def set_done_works(self, project_id: int, done_works: list):
        pass

    def set_project_info(self, project_id: int, info: tuple):
        pass

    def get_works_in_project(self, project_id: int):
        pass

    def get_resources_on_work(self, project_id: int, work_id: int):
        pass


class DataWorkerMock(DataWorker):
    def __init__(self):
        self.users = {}
        self.projects = {}
        self.works = {}
        self.workers = {}
        self.assignments = {}
        self.resources_on_projects = {}
        self.resources_on_base = {}

    def get_user(self, user_id: int):
        return self.users[user_id]

    def get_user_rights(self, user_id: int):
        # print(str(self.users))
        return self.users[user_id].get_rights()

    def add_user_rights_for_project(self, user_id: int, object_id: int, rights: dict):
        for r in rights:
            self.users[user_id].rights.add_rights_for_projects(self.projects[object_id], r, rights[r])

    def add_user_rights_for_work(self, user_id: int, object_id: int, rights: dict):
        for r in rights:
            self.users[user_id].rights.add_rights_for_works(self.works[object_id], r, rights[r])

    def add_user_rights_for_assignment(self, user_id: int, object_id: int, rights: dict):
        for r in rights:
            self.users[user_id].rights.add_rights_for_assigment(self.assignments[object_id], r, rights[r])

    def add_user_rights_for_resource(self, user_id: int, object_id: int, rights: dict):
        for r in rights:
            self.users[user_id].rights.add_rights_for_resource(self.resources_on_projects[object_id], r, rights[r])

    def set_work_plan(self, user_id: int, project_id: int, work_plan: list, rights: SystemRights):
        self.projects[project_id].set_work_plan(work_plan, rights)
        for w in work_plan:
            if len(self.works) == 0:
                next_key = 0
            else:
                next_key = max(list(self.works.keys())) + 1
            self.works[next_key] = w
            self.add_user_rights_for_work(user_id, next_key, {'description': Access.ALLOWED,
                                                              'resources': Access.ALLOWED,
                                                              'assignments': Access.ALLOWED,
                                                              'times': Access.ALLOWED,
                                                              'works': Access.ALLOWED})
            self.resources_on_projects[project_id][next_key] = {}

    def add_project(self, project: Project):
        if len(self.projects) == 0:
            new_id = 0
        else:
            new_id = max(list(self.projects.keys())) + 1
        self.projects[new_id] = project
        self.assignments[new_id] = {}
        self.resources_on_projects[new_id] = {}
        return new_id

    def get_worker(self, worker_id: int):
        return self.workers[worker_id]

    def get_resource_on_project(self, project_id: int, work_int: int, resource_id: int):
        # print('get_resource_on_project = ' + str(self.resources_on_projects))
        return self.resources_on_projects[project_id][work_int][resource_id]

    def get_work(self, work_id: int):
        return self.works[work_id]

    def get_project(self, project_id: int):
        return self.projects[project_id]

    def get_work_id(self, work: Work):
        return list(self.works.keys())[list(self.works.values()).index(work)]

    def get_resource_on_project_id(self, project_id: int, work_id: int, rs: Resource):
        # print(str(self.resources_on_projects[project_id][work_id].keys()))
        return list(self.resources_on_projects[project_id][work_id].keys())[list(self.resources_on_projects[project_id][work_id].values()).index(rs)]

    def add_assignment(self, project_id: int, worker_id: int, work_id: int, effort_units: float, rights: SystemRights):
        if len(self.assignments[project_id]) == 0:
            new_id = 0
        else:
            new_id = max(list(self.assignments[project_id].keys())) + 1
        worker = self.workers[worker_id]
        work = self.works[work_id]
        self.projects[project_id].add_worker(worker, work, effort_units, rights)

        assignment = Assignment(worker, effort_units)
        self.assignments[project_id][new_id] = assignment

    def add_resource_to_project(self, project_id: int, work_id: int, resource_id: int, quantity: int, rights: SystemRights):
        work = self.works[work_id]
        if quantity <= 0:
            raise LogicError('Quantity cannot be less or equal to 0')

        res = copy(self.resources_on_base[resource_id])
        if quantity <= self.resources_on_base[resource_id].quantity_in_storage:
            self.resources_on_base[resource_id].quantity_in_storage -= quantity
            rq = ResourceQuantity(res, quantity)
            self.projects[project_id].add_resource(res, quantity, work, rights)
            self.resources_on_projects[project_id][work_id][resource_id] = rq
        else:
            raise LogicError('You are trying to add ' + str(quantity) + ' resources, storage has ' + str(res.quantity))

    def get_resource_on_base(self, res_id: int):
        return self.resources_on_base[res_id]

    def set_resource_on_project(self, project_id: int, work_id: int, resource_id: int, quantity: int, spent: int,
                                rights: SystemRights):
        resource = self.resources_on_projects[project_id][work_id][resource_id]
        rq = ResourceQuantity(resource, quantity)
        rq.spent_quantity = spent
        self.works[work_id].set_resource(rq, rights)
        self.resources_on_projects[project_id][work_id][resource_id] = rq

    def set_work_real_start(self, work_id: int, date_time: datetime, rights: SystemRights):
        self.works[work_id].set_real_start(date_time, rights)

    def set_work_real_end(self, work_id: int, date_time: datetime, rights: SystemRights):
        self.works[work_id].set_real_end(date_time, rights)

    def set_done_works(self, project_id: int, done_works: list, rights: SystemRights):
        works = []
        for w in self.works:
            if w in done_works:
                works.append(self.works[w])
        self.projects[project_id].add_done_work(works, rights)

    def set_project_info(self, project_id: int, info: tuple, rights: SystemRights):
        self.projects[project_id].set_info(info, rights)

    def get_works_in_project(self, project_id: int):
        work_plan = self.projects[project_id].work_plan
        works_ids = []
        for w_id in self.works:
            if self.works[w_id] in work_plan:
                works_ids.append(w_id)
        return works_ids

    def get_resources_on_work(self, project_id: int, work_id: int):
        # print('project_id = ' + str(project_id))
        # print('work_id = ' + str(work_id))
        # print('self.resources_on_projects = ' + str(self.resources_on_projects))
        return list(self.resources_on_projects[project_id][work_id].keys())


class DataWorkerPostgreSQL(DataWorker):
    def __init__(self, db_path):
        self.db = postgresql.open(db_path)

    def get_user(self, user_id: int):
        if not self.user_exists(user_id):
            raise LogicError("User with ID " + str(user_id) + " doesn't exist")

        user_db = self.db.query("SELECT fullname, login, password FROM users WHERE user_id = $1;", user_id)[0]
        user = SystemUser(user_db['fullname'].strip(), user_db['login'].strip(), user_db['password'].strip())

        rights_db = self.db.query("SELECT object_type, object_id, action_id, access_id FROM rights WHERE user_id = $1;", user_id)
        if len(rights_db) != 0:
            for right in rights_db:
                if right['object_type'].strip() == 'project':
                    project = self.get_project(right['object_id'])
                    user.rights.add_rights_for_projects(project, right['action_id'].strip(), Access(right['access_id']))
                if right['object_type'].strip() == 'work':
                    work = self.get_work(right['object_id'])
                    user.rights.add_rights_for_works(work, right['action_id'].strip(), Access(right['access_id']))

        return user

    def get_user_rights(self, user_id: int):
        if not self.user_exists(user_id):
            raise LogicError("User with ID " + str(user_id) + " doesn't exist")

        rights = SystemRights()
        rights_db = self.db.query("SELECT object_type, object_id, action_id, access_id FROM rights WHERE user_id = $1;", user_id)
        if len(rights_db) != 0:
            for right in rights_db:
                if right['object_type'].strip() == 'project':
                    project = self.get_project(right['object_id'])
                    rights.add_rights_for_projects(project, right['action_id'].strip(), Access(right['access_id']))
                if right['object_type'].strip() == 'work':
                    work = self.get_work(right['object_id'])
                    rights.add_rights_for_works(work, right['action_id'].strip(), Access(right['access_id']))

        return rights

    def add_user_rights_for_project(self, user_id: int, object_id: int, rights: dict):
        if not self.user_exists(user_id):
            raise LogicError("User with ID " + str(user_id) + " doesn't exist")
        if not self.project_exists(object_id):
            raise LogicError("Project with ID " + str(object_id) + " doesn't exist")
        for action, access in rights.items():
            self.db.query("""
                INSERT INTO rights (
                    user_id,
                    object_type,
                    object_id,
                    action_id,
                    access_id)
                VALUES ($1, $2, $3, $4, $5);
                """, user_id, 'project', object_id, action, access.value)

    def add_user_rights_for_work(self, user_id: int, object_id: int, rights: dict):
        if not self.user_exists(user_id):
            raise LogicError("User with ID " + str(user_id) + " doesn't exist")
        if not self.work_exists(object_id):
            raise LogicError("Work with ID " + str(object_id) + " doesn't exist")
        for action, access in rights.items():
            self.db.query("""
                INSERT INTO rights (
                    user_id,
                    object_type,
                    object_id,
                    action_id,
                    access_id)
                VALUES ($1, $2, $3, $4, $5);
                """, user_id, 'work', object_id, action, access.value)

    def set_work_plan(self, user_id: int, project_id: int, work_plan: list, rights: SystemRights):
        project = self.get_project(project_id)
        project.set_work_plan(work_plan, rights)
        self.update_project(project, project_id)

    def add_project(self, project: Project):
        # Project
        manager_db = self.db.query("SELECT user_id FROM users WHERE login = $1;", project.manager.login)
        manager_id = manager_db[0]['user_id']
        project_id_db = self.db.query("""
            INSERT INTO projects (
                manager_id,
                customer,
                start_of_construction,
                deadline_of_construction,
                description)
            VALUES ($1, $2, $3, $4, $5)
            RETURNING project_id;
            """,
            manager_id, project.customer,
            project.start_of_construction.strftime('%d.%m.%Y'), project.deadline_of_construction.strftime('%d.%m.%Y'),
            project.description)
        project_id = project_id_db[0]['project_id']

        # Brigadiers
        for brigadier in project.brigadiers:
            self.db.query("""
                INSERT INTO brigadiers (
                    project_id,
                    effort,
                    fullname,
                    salary)
                VALUES ($1, $2, $3, $4);
                """, project_id, 0, brigadier.fullname, brigadier.salary)

        # Current works
        for work in project.work_plan:
            work_id_db = self.db.query("""
                INSERT INTO works (
                    project_id,
                    name,
                    status,
                    start_of_realization,
                    deadline)
                VALUES ($1, $2, $3, $4, $5)
                RETURNING work_id;
                """,
                project_id, work.name, 'in-plan',
                work.start_of_realization.strftime('%d.%m.%Y, %H:%M'), work.deadline.strftime('%d.%m.%Y, %H:%M'))
            work_id = work_id_db[0]['work_id']
            # Workers
            for assignment in work.assignments:
                self.db.query("""
                    INSERT INTO workers (
                        work_id,
                        effort,
                        fullname,
                        salary)
                    VALUES ($1, $2, $3, $4);
                    """, work_id, assignment.effort_units, assignment.worker.fullname, assignment.worker.salary)

        # Done works
        for work in project.done_work:
            work_id = self.get_work_id(work, project_id)
            self.db.query("""
                UPDATE works SET
                    status = 'done'
                WHERE work_id = $1 AND project_id = $2;
                """, work_id, project_id)

        # Add works relationships
        all_works = []
        all_works.extend(project.work_plan)
        all_works.extend(project.done_work)
        for work in all_works:
            work_id = self.get_work_id(work, project_id)

            works_ss = []
            for w in work.works_start_to_start:
                works_ss.append(self.get_work_id(w, project_id))
            works_fs = []
            for w in work.works_finish_to_start:
                works_fs.append(self.get_work_id(w, project_id))
            works_ff = []
            for w in work.works_finish_to_finish:
                works_ff.append(self.get_work_id(w, project_id))
            works_sf = []
            for w in work.works_start_to_finish:
                works_sf.append(self.get_work_id(w, project_id))

            self.db.query("""
                UPDATE works SET
                    works_ss = $1,
                    works_fs = $2,
                    works_ff = $3,
                    works_sf = $4
                WHERE work_id = $5 AND project_id = $6;
                """, str(works_ss), str(works_fs), str(works_ff), str(works_sf), work_id, project_id)

        return project_id

    def update_project(self, project: Project, project_id):
        if not self.project_exists(project_id):
            raise LogicError("Project with ID " + str(project_id) + " doesn't exist")

        # Delete old information
        self.db.query("DELETE FROM projects WHERE project_id = $1;", project_id)
        self.db.query("DELETE FROM brigadiers WHERE project_id = $1;", project_id)
        self.db.query("DELETE FROM rights WHERE object_type = $1 AND object_id = $2;", 'project', project_id)

        works_db = self.db.query("SELECT work_id FROM works WHERE project_id = $1;", project_id)
        for work_db in works_db:
            self.db.query("DELETE FROM workers WHERE work_id = $1;", work_db['work_id'])

        self.db.query("DELETE FROM works WHERE project_id = $1;", project_id)

        # Add new project
        new_project_id = self.add_project(project)

        return new_project_id

    def get_project(self, project_id: int):
        if not self.project_exists(project_id):
            raise LogicError("Project with ID " + str(project_id) + " doesn't exist")

        # Get information about project
        project_db = self.db.query("""
            SELECT manager_id, customer, start_of_construction, deadline_of_construction, description
            FROM projects 
            WHERE project_id = $1;""", project_id)[0]

        # Get information about manager
        manager_db = self.db.query("SELECT fullname, login, password FROM users WHERE user_id = $1;",
                                   project_db['manager_id'])[0]

        # Create manager
        manager = SystemUser(manager_db['fullname'].strip(), manager_db['login'].strip(), manager_db['password'].strip())

        # Create project
        project = Project(manager, project_db['customer'].strip(),
                          project_db['start_of_construction'].strip(), project_db['deadline_of_construction'].strip())
        project.set_description(project_db['description'].strip(), SystemRights(
                                rights_for_projects=[SystemRightsForObject(project, {'description': Access.ALLOWED})]))

        # Get information about brigadiers
        brigadiers_db = self.db.query("SELECT fullname, salary FROM brigadiers WHERE project_id = $1;", project_id)

        # Add brigadiers to project
        for brigadier_db in brigadiers_db:
            brigadier = Worker(brigadier_db['fullname'].strip(), brigadier_db['salary'])
            project.add_brigadier(brigadier, SystemRights(
                rights_for_projects=[SystemRightsForObject(project, {'brigadiers': Access.ALLOWED})]))

        # Get information about works
        works_db = self.db.query("""
            SELECT work_id, name, status, start_of_realization, deadline, works_ss, works_fs, works_ff, works_sf
            FROM works
            WHERE project_id = $1;""", project_id)

        # Add works and assignments to project
        for work_db in works_db:
            work = Work(work_db['name'].strip(), work_db['start_of_realization'].strip(), work_db['deadline'].strip())

            for work_id in ast.literal_eval(work_db['works_ss'].strip()):
                work.works_start_to_start.append(self.get_work(work_id))
            for work_id in ast.literal_eval(work_db['works_fs'].strip()):
                work.works_finish_to_start.append(self.get_work(work_id))
            for work_id in ast.literal_eval(work_db['works_ff'].strip()):
                work.works_finish_to_finish.append(self.get_work(work_id))
            for work_id in ast.literal_eval(work_db['works_sf'].strip()):
                work.works_start_to_finish.append(self.get_work(work_id))

            # Get workers which assigned to this work
            workers_db = self.db.query("SELECT effort, fullname, salary FROM workers WHERE work_id = $1;", work_db['work_id'])
            for worker_db in workers_db:
                worker = Worker(worker_db['fullname'].strip(), worker_db['salary'])
                work.add_assignment(Assignment(worker, worker_db['effort']), SystemRights(
                    rights_for_works=[SystemRightsForObject(work, {'assignments': Access.ALLOWED})]))
            project.add_work(work, SystemRights(rights_for_projects=[SystemRightsForObject(project, {'work': Access.ALLOWED})]))

            # Add done works to done list
            if work_db['status'].strip() == 'done':
                project.add_done_work([work], SystemRights(rights_for_projects=[SystemRightsForObject(project, {'done_work': Access.ALLOWED})]))

        return project

    def get_work(self, work_id: int):
        # Get information about work
        work_db = self.db.query("""
                    SELECT work_id, name, start_of_realization, deadline
                    FROM works
                    WHERE work_id = $1;""", work_id)[0]

        work = Work(work_db['name'].strip(), work_db['start_of_realization'].strip(), work_db['deadline'].strip())
        # Get workers which assigned to this work
        workers_db = self.db.query("SELECT effort, fullname, salary FROM workers WHERE work_id = $1;",
                                   work_db['work_id'])
        for worker_db in workers_db:
            worker = Worker(worker_db['fullname'].strip(), worker_db['salary'])
            work.add_assignment(Assignment(worker, worker_db['effort']), SystemRights(
                rights_for_works=[SystemRightsForObject(work, {'assignments': Access.ALLOWED})]))

        return work

    def get_work_id(self, work: Work, project_id):
        return self.db.query("SELECT work_id FROM works WHERE name = $1 AND project_id = $2;", work.name, project_id)[0]['work_id']

    def set_done_works(self, project_id: int, done_works_ids: list):
        if not self.project_exists(project_id):
            raise LogicError("Project with ID " + str(project_id) + " doesn't exist")
        for work_id in done_works_ids:
            self.db.query("""
                UPDATE works SET
                    status = 'done'
                WHERE work_id = $1 AND project_id = $2;
                """, work_id, project_id)

    def set_project_info(self, project_id: int, info: tuple):
        if not self.project_exists(project_id):
            raise LogicError("Project with ID " + str(project_id) + " doesn't exist")
        self.db.query("""
            UPDATE projects SET
                customer = $1,
                start_of_construction = $2,
                deadline_of_construction = $3,
                description = $4
            WHERE project_id = $5;
            """, info[0], info[1], info[2], info[3], project_id)

    def get_works_in_project(self, project_id: int):
        if not self.project_exists(project_id):
            raise LogicError("Project with ID " + str(project_id) + " doesn't exist")
        works_db = self.db.query("SELECT work_id FROM works "
                                 "WHERE project_id = $1 AND status = 'in-plan';", project_id)
        works_ids = []
        for work in works_db:
            works_ids.append(work['work_id'])
        return works_ids

    def project_exists(self, project_id):
        project_db = self.db.query("SELECT * FROM projects WHERE project_id = $1;", project_id)
        if len(project_db) == 0:
            return False
        else:
            return True

    def user_exists(self, user_id):
        user_db = self.db.query("SELECT * FROM users WHERE user_id = $1;", user_id)
        if len(user_db) == 0:
            return False
        else:
            return True

    def work_exists(self, work_id):
        work_db = self.db.query("SELECT * FROM works WHERE work_id = $1;", work_id)
        if len(work_db) == 0:
            return False
        else:
            return True

