from PyQt4 import QtGui
import sys

import ui.window
from src.business_components.create_works import *


class App(QtGui.QMainWindow, ui.window.Ui_MainWindow):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)
        self.setFixedSize(1200, 500)
        self.data_worker = DataWorkerPostgreSQL('pq://rufus@localhost:5432/db')
        works, dates = get_works(self.data_worker, 1)
        i = 0
        for work in works:
            self.button = QtGui.QPushButton('Test', self)
            self.button.setGeometry(work[0] + 90,10 + i,work[1] - work[0],50)
            self.button.setText(work[2])
            self.label = QtGui.QLabel(dates[work[0]], self)
            self.label.setGeometry(work[0] + 15, 10 + i + 15, work[0] + 50, 20)
            self.label2 = QtGui.QLabel(dates[work[1]], self)
            self.label2.setGeometry(work[1] + 100, 10 + i + 15, 100, 20)
            i = i + 50

        # i = 0
        # for key, value in dates.items():
        #     self.label = QtGui.QLabel(value, self)
        #     self.label.setGeometry(key, 10 + i, key + 50, 20)
        #     i = i + 50


def main():
    app = QtGui.QApplication(sys.argv)
    form = App()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
