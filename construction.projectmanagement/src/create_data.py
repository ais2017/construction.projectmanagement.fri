from src.utils.data_worker import *
from src.classes.project import *
from src.classes.system import *
from src.classes.work import *
from src.classes.worker import *
from copy import copy


data_worker = DataWorkerMock()

manager1 = SystemUser('Main Manager', 'MM', 'howyoudoin')
data_worker.users[1] = manager1

pr1 = Project(manager1, 'Serious Customer', '10.10.2017', '10.10.2018')

data_worker.add_project(pr1)

manager1_rights = SystemRights()
manager1_rights.add_rights_for_projects(pr1, 'description', Access.ALLOWED)
manager1_rights.add_rights_for_projects(pr1, 'brigadiers', Access.ALLOWED)
manager1_rights.add_rights_for_projects(pr1, 'work', Access.ALLOWED)
manager1_rights.add_rights_for_projects(pr1, 'done_work', Access.FORBIDDEN)
manager1.set_rights(manager1_rights, {'general': Access.ALLOWED})

data_worker.workers[1] = Worker('Lev Tolstoy', 100)
data_worker.workers[2] = Worker('Hugh Jackman', 200)
data_worker.workers[3] = Worker('Kseniya Sobchak', 50)

data_worker.resources_on_base[0] = Resource(100)
data_worker.resources_on_base[1] = Resource(100)

pr3 = Project(manager1, 'Hollywood', '10.10.2017', '10.10.2040')
pr3_id = data_worker.add_project(pr3)

data_worker.users[1].rights.add_rights_for_projects(pr3, 'description', Access.ALLOWED)
data_worker.users[1].rights.add_rights_for_projects(pr3, 'brigadiers', Access.ALLOWED)
data_worker.users[1].rights.add_rights_for_projects(pr3, 'work', Access.ALLOWED)
data_worker.users[1].rights.add_rights_for_projects(pr3, 'done_work', Access.FORBIDDEN)

w1 = Work('Painting', '10.12.2017, 09:00', '11.12.2018, 09:00')
w2 = Work('Laying of tiles', '11.12.2017, 09:00', '15.12.2018, 09:00')
data_worker.set_work_plan(1, pr3_id, [w1, w2], data_worker.users[1].get_rights())
# w1.set_work_after(w2, data_worker)
works_ids = []
for w_id in list(data_worker.works.keys()):
    if data_worker.works[w_id] == w1 or data_worker.works[w_id] == w2:
        works_ids.append(w_id)

data_worker.users[1].rights.add_rights_for_works(works_ids[0], 'resources', Access.ALLOWED)
data_worker.users[1].rights.add_rights_for_works(works_ids[1], 'resources', Access.ALLOWED)

# print('pr3_id = ' + str(pr3_id))
data_worker.add_resource_to_project(pr3_id, works_ids[0], 0, 10, data_worker.users[1].rights)
data_worker.add_resource_to_project(pr3_id, works_ids[1], 1, 20, data_worker.users[1].rights)


br1 = SystemUser('Main Brigadier', 'petrovich', 'vodka')
data_worker.users[2] = br1

pr4 = Project(manager1, 'Trump', '10.08.2017', '10.11.2300')
pr4_id = data_worker.add_project(pr4)
# print('pr4_id = ' + str(pr4_id))

data_worker.users[1].rights.add_rights_for_projects(pr4, 'description', Access.ALLOWED)
data_worker.users[1].rights.add_rights_for_projects(pr4, 'brigadiers', Access.ALLOWED)
data_worker.users[1].rights.add_rights_for_projects(pr4, 'work', Access.ALLOWED)

w3 = Work('Dancing', '10.10.2017, 09:00', '11.10.2017, 09:00')
w4 = Work('Singing', '11.10.2017, 09:00', '10.10.2018, 09:00')

data_worker.users[1].rights.add_rights_for_works(w3, 'works', Access.ALLOWED)
data_worker.users[1].rights.add_rights_for_works(w4, 'works', Access.ALLOWED)

data_worker.set_work_plan(1, pr4_id, [w3, w4], data_worker.users[1].rights)

pr4.work_plan[0].add_work_after(pr4.work_plan[1], data_worker.users[1].get_rights())
# print('pr4.work_plan[0] after works = ' + str(pr4.work_plan[0].works_finish_to_start))

pr4_works = data_worker.get_works_in_project(pr4_id)
# print('pr4_works = ' + str(pr4_works))
# for w in pr4_works:
#     print('AAAAA = ' + str(data_worker.get_work(w).works_finish_to_start))

br1.rights.add_rights_for_projects(pr4, 'done_work', Access.ALLOWED)
for w in pr4.work_plan:
    br1.rights.add_rights_for_works(w, 'times', Access.ALLOWED)

boss1 = SystemUser('Big Boss', 'big_boss', 'secret_password')
boss1.rights = copy(manager1.rights)
data_worker.users[3] = boss1


pr5 = Project(manager1, 'Yandex', '10.08.2017', '10.11.2300')
pr5_id = data_worker.add_project(pr5)

data_worker.users[1].rights.add_rights_for_projects(pr5, 'description', Access.ALLOWED)
data_worker.users[1].rights.add_rights_for_projects(pr5, 'brigadiers', Access.ALLOWED)
data_worker.users[1].rights.add_rights_for_projects(pr5, 'work', Access.ALLOWED)

w5_1 = Work('Painting 2', '10.12.2017, 09:00', '11.12.2018, 09:00')
w5_2 = Work('Laying of tiles 2', '11.12.2017, 09:00', '15.12.2018, 09:00')
data_worker.set_work_plan(1, pr5_id, [w5_1, w5_2], data_worker.users[1].get_rights())
works_ids = []
for w_id in list(data_worker.works.keys()):
    if data_worker.works[w_id] == w5_1 or data_worker.works[w_id] == w5_2:
        works_ids.append(w_id)

data_worker.users[1].rights.add_rights_for_works(works_ids[0], 'resources', Access.ALLOWED)
data_worker.users[1].rights.add_rights_for_works(works_ids[1], 'resources', Access.ALLOWED)

# print('pr3_id = ' + str(pr3_id))
data_worker.add_resource_to_project(pr5_id, works_ids[0], 0, 10, data_worker.users[1].rights)
data_worker.add_resource_to_project(pr5_id, works_ids[1], 1, 20, data_worker.users[1].rights)

