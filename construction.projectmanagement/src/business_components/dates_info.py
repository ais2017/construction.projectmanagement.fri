from src.utils.data_worker import *
from src.business_components.creation_gantt_diagram import *


class DatesInfo:
    def __init__(self, data_worker: DataWorker):
        self.data_worker = data_worker

    def get_dates_info(self, project_id: int):
        project = self.data_worker.get_project(project_id)
        return project.get_start_of_construction(), project.get_deadline_of_construction()

    def set_start_of_construction(self, project_id: int, user_id: int, date: str):
        user_rights = self.data_worker.get_user_rights(user_id)
        self.data_worker.get_project(project_id).set_start_of_construction(date, user_rights)

    def set_deadline_of_construction(self, project_id: int, user_id: int, date: str):
        user_rights = self.data_worker.get_user_rights(user_id)
        self.data_worker.get_project(project_id).set_deadline_of_construction(date, user_rights)

    def create_work_plan(self, user_id: int, project_id: int, works_descriptions: list, works_connections: list):
        cr_gantt = CreateGanttDiagram(self.data_worker)
        cr_gantt.create_gantt_diagram(user_id, project_id, works_descriptions, works_connections)
