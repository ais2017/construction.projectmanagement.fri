from src.business_components.creation_gantt_diagram import *


class CreateProject:
    def __init__(self, data_worker: DataWorker):
        self.data_worker = data_worker

    def fill_project_fields(self, manager_id: int, customer: str = '',
                            start_of_construction: str = '01.01.1971', deadline_of_construction: str = '01.01.1971'):

        project = Project(self.data_worker.get_user(manager_id), customer,
                          start_of_construction, deadline_of_construction)
        project_id = self.data_worker.add_project(project)

        self.data_worker.add_user_rights_for_project(manager_id, project_id,
                                                     {'description': Access.ALLOWED,
                                                      'brigadiers': Access.ALLOWED,
                                                      'work': Access.ALLOWED,
                                                      'done_work': Access.FORBIDDEN})

        return project_id

    def create_work_plan(self, user_id: int, project_id: int, works_descriptions: list, works_connections: list):
        cr_gantt = CreateGanttDiagram(self.data_worker)
        cr_gantt.create_gantt_diagram(user_id, project_id, works_descriptions, works_connections)

    def create_assignment(self, user_id: int, project_id: int, worker_id: int, work_id: int, effort_units: float = 0.0):
        user_rights = self.data_worker.get_user_rights(user_id)
        self.data_worker.add_assignment(project_id, worker_id, work_id, effort_units, user_rights)

    def add_resource(self, user_id: int, project_id: int, resource_id: int, quantity: int, work_id: int):
        user_rights = self.data_worker.get_user_rights(user_id)
        self.data_worker.add_resource_to_project(project_id, work_id, resource_id, quantity, user_rights)
