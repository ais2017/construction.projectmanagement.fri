from datetime import datetime

from src.utils.data_worker import *
from src.classes.project import *
from src.classes.system import *
from src.classes.work import *
from src.classes.worker import *
from copy import copy

def delta(dt1, dt2):
    return int(dt1.strftime("%s")) * 1000 - int(dt2.strftime("%s")) * 1000


def get_works(data_worker, project_id):

    project_extracted = data_worker.get_project(project_id)

    works = project_extracted.work_plan

    # works = []
    # works.extend([w2, w1, w3, w4, w5, w6, w7])

    works_tuples = []
    for work in works:
        works_tuples.append((work.start_of_realization, work.deadline, work.name))

    works_tuples = sorted(works_tuples)

    works_shifted = []
    for w in works_tuples:
        works_shifted.append((delta(w[0], works_tuples[0][0]),
                              delta(w[1], works_tuples[0][0]),
                              w[2]))

    m = max(works_shifted, key=lambda item:item[1])[1]
    k = 1000 / m

    works_normal = []
    d = {}
    i = 0
    for w in works_shifted:
        works_normal.append((int(w[0] * k), int(w[1] * k), w[2]))
        d[int(w[0] * k)] = works_tuples[i][0].strftime('%d.%m.%Y')
        d[int(w[1] * k)] = works_tuples[i][1].strftime('%d.%m.%Y')
        i = i + 1

    return works_normal, d


# res, d = get_works()
# print(res)
# print(d)