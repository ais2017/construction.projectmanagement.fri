from src.utils.data_worker import *


class ProjectDelays:
    def __init__(self, data_worker: DataWorker):
        self.data_worker = data_worker

    def get_project_delay(self, project_id: int):
        project = self.data_worker.get_project(project_id)
        return project.calculate_delay()

    def get_project_deadline(self, project_id: int):
        project = self.data_worker.get_project(project_id)
        return project.get_deadline_of_construction()

    def get_works_delays(self, project_id: int):
        project = self.data_worker.get_project(project_id)
        work_plan = project.get_works_plan()
        works_delays = {}
        for w in work_plan:
            if w.get_delay() != timedelta(0):
                works_delays[self.data_worker.get_work_id(w)] = w.get_delay()
        return works_delays
