from src.utils.data_worker import *


class GeneralInfo:
    def __init__(self, data_worker):
        self.data_worker = data_worker

    def get_all_accessible_projects(self, user_id: int):
        user_rights = self.data_worker.get_user_rights(user_id)
        accessible_projects = []
        for p_id in self.data_worker.projects:
            project = self.data_worker.get_project(p_id)
            p_rights = user_rights.get_project_accesses(project)
            if Access.ALLOWED in list(p_rights.values()):
                accessible_projects.append(p_id)
        return accessible_projects

    def get_project_info(self, project_id: int):
        project = self.data_worker.get_project(project_id)
        return project.get_view_info()

    def change_project_info(self, user_id: int, project_id: int, info: tuple):
        user_rights = self.data_worker.get_user_rights(user_id)
        self.data_worker.set_project_info(project_id, info, user_rights)
