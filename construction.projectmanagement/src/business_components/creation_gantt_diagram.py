from src.classes.work import *
from src.utils.data_worker import *
from copy import copy


class CreateGanttDiagram:
    def __init__(self, data_worker: DataWorker):
        self.data_worker = data_worker

    # works_descriptions is the list of work description from which can be build work object by using constructor
    # works_connections is the list where each index correspond to work in works_descriptions list and element contain
    # 4 dicts of corresponding works (their indexes actually) — works_start_to_start, works_finish_to_start,
    # works_finish_to_finish and works_start_to_finish
    def create_gantt_diagram(self, user_id: int, project_id: int, works_descriptions: list, works_connections: list):
        rights = self.data_worker.get_user_rights(user_id)
        rights_for_works = copy(rights)

        work_plan = {}
        for i, w in enumerate(works_descriptions):
            work = Work(*w)
            work_plan[i] = work
            rights_for_works.add_rights_for_works(work_plan[i], 'works', Access.ALLOWED)

        for i, wc in enumerate(works_connections):
            if 'ss' in wc:
                for ss in wc['ss']:
                    work_plan[i].add_work_parallel_start(work_plan[ss], rights_for_works)
            if 'fs' in wc:
                for fs in wc['fs']:
                    work_plan[i].add_work_after(work_plan[fs], rights_for_works)
            if 'ff' in wc:
                for ff in wc['ff']:
                    work_plan[i].add_work_parallel_end(work_plan[ff], rights_for_works)
            if 'sf' in wc:
                for sf in wc['sf']:
                    work_plan[i].add_work_before(work_plan[sf], rights_for_works)

        self.data_worker.set_work_plan(user_id, project_id, list(work_plan.values()), rights)
