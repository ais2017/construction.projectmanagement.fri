from src.utils.data_worker import *


class ResourcesPlan:
    def __init__(self, data_worker: DataWorker):
        self.data_worker = data_worker

    def get_resources_info(self, project_id: int):
        out_resources_info = {}
        works_ids = self.data_worker.get_works_in_project(project_id)
        # print('works_ids = ' + str(works_ids))
        for w_id in works_ids:
            out_resources_info[w_id] = []
            w_ress = self.data_worker.get_resources_on_work(project_id, w_id)
            # print('w_ress = ' + str(w_ress))
            for r_id in w_ress:
                res_on_project = self.data_worker.get_resource_on_project(project_id, w_id, r_id)
                out_resources_info[w_id].append((r_id, res_on_project.quantity, res_on_project.spent_quantity))

        return out_resources_info

    def change_resource(self, project_id: int, user_id: int, work_id: int, resource_id: int, quantity: int, spent: int):
        user_rights = self.data_worker.get_user_rights(user_id)
        self.data_worker.set_resource_on_project(project_id, work_id, resource_id, quantity, spent, user_rights)

    def add_resource(self, project_id: int, user_id: int, work_id: int, resource_id: int, quantity: int):
        user_rights = self.data_worker.get_user_rights(user_id)
        self.data_worker.add_resource_to_project(project_id, work_id, resource_id, quantity, user_rights)
