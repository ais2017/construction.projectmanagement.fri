from src.utils.data_worker import *


class DoneWorks:
    def __init__(self, data_worker: DataWorker):
        self.data_worker = data_worker

    def get_project_info(self, project_id: int):
        project = self.data_worker.get_project(project_id)
        return project.get_view_info()

    def get_real_work_plan(self, project_id: int):
        works_connections = {}
        works = {}
        work_ids = self.data_worker.get_works_in_project(project_id)
        for w_id in work_ids:
            work = self.data_worker.get_work(w_id)

            works[w_id] = (work.get_name(), work.get_start_of_realization().strftime('%d.%m.%Y, %H:%M'),
                           work.get_deadline(),
                           work.get_real_start(), work.get_real_end())

            works_connections[w_id] = {}
            works_connections[w_id]['ss'] = []
            works_connections[w_id]['sf'] = []
            works_connections[w_id]['ff'] = []
            works_connections[w_id]['fs'] = []

            for w_id_ in work_ids:
                if self.data_worker.get_work(w_id_) in work.works_start_to_start:
                    works_connections[w_id]['ss'].append(w_id_)
                if self.data_worker.get_work(w_id_) in work.works_start_to_finish:
                    works_connections[w_id]['sf'].append(w_id_)
                if self.data_worker.get_work(w_id_) in work.works_finish_to_finish:
                    works_connections[w_id]['ff'].append(w_id_)
                if self.data_worker.get_work(w_id_) in work.works_finish_to_start:
                    works_connections[w_id]['fs'].append(w_id_)

        return works, works_connections

    def set_real_start(self, user_id: int, work_id: int, date_time: str):
        user_rights = self.data_worker.get_user_rights(user_id)
        self.data_worker.set_work_real_start(work_id, datetime.strptime(date_time, '%d.%m.%Y, %H:%M'), user_rights)

    def set_real_end(self, user_id: int, work_id: int, date_time: str):
        user_rights = self.data_worker.get_user_rights(user_id)
        self.data_worker.set_work_real_end(work_id, datetime.strptime(date_time, '%d.%m.%Y, %H:%M'), user_rights)

    def add_done_works(self, user_id: int, project_id: int, done_works: list):
        user_rights = self.data_worker.get_user_rights(user_id)
        self.data_worker.set_done_works(project_id, done_works, user_rights)
