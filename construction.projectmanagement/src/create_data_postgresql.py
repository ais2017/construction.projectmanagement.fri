from src.utils.data_worker import *
from src.classes.project import *
from src.classes.system import *
from src.classes.work import *
from src.classes.worker import *
from copy import copy


data_worker = DataWorkerPostgreSQL('pq://rufus@localhost:5432/db')

manager = SystemUser('Tony Stark', 'ironman', 'password')

prj = Project(manager, 'Serious Customer', '10.10.2017', '10.10.2018')

w1 = Work('Dancing', '05.12.2017, 09:00', '21.12.2017, 09:00')
w2 = Work('Singing', '05.12.2017, 09:00', '12.12.2017, 09:00')
w3 = Work('Painting', '11.12.2017, 09:00', '20.12.2017, 09:00')
w4 = Work('Fighting', '08.12.2017, 09:00', '17.12.2017, 09:00')
w5 = Work('Lightning', '01.12.2017, 09:00', '11.12.2017, 09:00')
w6 = Work('Talking', '09.12.2017, 09:00', '16.12.2017, 09:00')
w7 = Work('Hacking', '09.12.2017, 09:00', '17.12.2017, 09:00')

prj.add_work(w1, SystemRights(rights_for_projects=[SystemRightsForObject(prj, {'work': Access.ALLOWED})]))
prj.add_work(w2, SystemRights(rights_for_projects=[SystemRightsForObject(prj, {'work': Access.ALLOWED})]))
prj.add_work(w3, SystemRights(rights_for_projects=[SystemRightsForObject(prj, {'work': Access.ALLOWED})]))
prj.add_work(w4, SystemRights(rights_for_projects=[SystemRightsForObject(prj, {'work': Access.ALLOWED})]))
prj.add_work(w5, SystemRights(rights_for_projects=[SystemRightsForObject(prj, {'work': Access.ALLOWED})]))
prj.add_work(w6, SystemRights(rights_for_projects=[SystemRightsForObject(prj, {'work': Access.ALLOWED})]))
prj.add_work(w7, SystemRights(rights_for_projects=[SystemRightsForObject(prj, {'work': Access.ALLOWED})]))

project_id = data_worker.add_project(prj)
